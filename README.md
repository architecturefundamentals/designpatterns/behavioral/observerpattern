# OBSERVER PATTERN


---
## A bit of context ...
>### _What is this pattern about?_
>This pattern it's a structural pattern, it lets you define a subscription mechanism to notify multiple objects about any events that happen to the object they’re observing.
>
> It's usually used in the next cases:
>  * when changes to the state of one object may require changing other objects, and the actual set of objects is unknown beforehand or changes dynamically.
>  * when some objects in your app must observe others, but only for a limited time or in specific cases.
> 
---
## But this simple example is about...

![Demo](img/Adapter Pattern-Observerv2.jpg)

and Done!
:smile:
---
_Tech: C#_ 