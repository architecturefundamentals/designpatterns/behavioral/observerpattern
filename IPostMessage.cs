﻿namespace DesignPatterns.Observer
{
    public interface IPostMessage
    {
        public void postMessage(Message message);
    }
}
