﻿namespace DesignPatterns.Observer
{
    public class MessagePublisher : IPublisher<Message>, IPostMessage
    {
        private List<IObserver<Message>> subscribers;

        public MessagePublisher()
        {
            subscribers = new List<IObserver<Message>>();
        }
        public void addGroupMember(IObserver<Message> subscriber)
        {
            this.subscribers.Add(subscriber);
        }

        public void notify(Message message)
        {
            foreach(IObserver<Message> sub in subscribers)
            {
                sub.execute(message);
            }
        }

        public void removeGroupMember(IObserver<Message> subscriber)
        {
            var member = (GroupMember)subscriber;
            Console.WriteLine($"{member.Name} has left the group");
            subscribers.Remove(subscriber);
        }

        public void postMessage(Message message)
        {
            notify(message);
        }
    }
}
