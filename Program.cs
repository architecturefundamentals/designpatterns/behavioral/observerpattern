﻿using DesignPatterns.Observer;


var sub1 = new GroupMember("John");
var sub2 = new GroupMember("Chavelo");
var sub3 = new GroupMember("Sergia");

MessagePublisher group = new MessagePublisher();

group.addGroupMember(sub1);
group.addGroupMember(sub2);
group.addGroupMember(sub3);

group.postMessage(new Message(sub1.Name, "Hello everyone!!!"));
group.removeGroupMember(sub1);

group.postMessage(new Message(sub2.Name, "La tuya por si acaso"));