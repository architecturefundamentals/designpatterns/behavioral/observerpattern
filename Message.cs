﻿namespace DesignPatterns.Observer
{
    public class Message
    {
        public string UserName { get; set; }
        public string Text { get; set; }
        public Message(string userName, string message)
        {
            UserName = userName;
            Text = message;
        }
    }
}
