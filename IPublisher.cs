﻿namespace DesignPatterns.Observer
{
    public interface IPublisher<T>
    {
        public void addGroupMember(IObserver<T> subscriber);
        public void removeGroupMember(IObserver<T> subscriber);
        public void notify(T message);
    }
}
